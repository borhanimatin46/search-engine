import com.google.common.base.Charsets;
import com.google.common.io.Files;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class IndexFile {
    String fileName ;
    File fileReader;
    public IndexFile(String fileName) {
        this.fileName = fileName;
        fileReader = new File(fileName);
    }

    public List canIndex(String [] wordList){
        ArrayList<String> newList = new ArrayList<>();
        for (int i = 0; i< wordList.length ; i++) {
            String strTemp = wordList[i];
            if (! strTemp.equals("") && !strTemp.equals(" ") && strTemp.length() > 3)
                newList.add(strTemp);

        }
        return newList;
    }
    public ArrayList<String> getAllWord() throws IOException {
        List<String> listWord = new ArrayList<>();
        List<String> stringList = Files.readLines(fileReader, Charsets.UTF_8);
        for (int i = 0 ; i<stringList.size() ; i++){
            String str = stringList.get(i);
            String [] wordList = str.split(" ");
             listWord = canIndex(wordList);
            System.out.println();
        }
        return null;
    }
}
