package file_handling;

import java.io.Serializable;

public class Properties implements Serializable {
    String url;
    String docID;
    int count;
//    int TFIDF;

    // ************************ Constructor

    public Properties(String url, String docID, int count) {
        this.url = url;
        this.docID = docID;
        this.count = count;
    }

    public Properties(String url, String docID) {
        this.url = url;
        this.docID = docID;
    }

    public Properties() {
    }

    @Override
    public String toString() {
        String str = url + "," + docID + "," + count;
        return str;
    }

//    public void TFIDF(){
//        // count
//        // formol
//    }
}

