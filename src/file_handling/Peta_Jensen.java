package file_handling;

import com.google.common.base.CharMatcher;
import com.google.common.base.Charsets;
import com.google.common.base.Splitter;
import com.google.common.io.Files;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

class Peta_Jensen {
    final int MIN_LENGTH=3;
    HashMap<String,List<Properties>> hashWords = new HashMap<>();
    /*
    Remove all words's length is 3 or less
     */
    public List<String> removeBadWords(List<String> listStr){
        List<String> listStrFinal = new ArrayList<>();
        for (int i = 0 ; i<listStr.size();i++) {
            String strTemp = listStr.get(i);

            if (strTemp.length() >= MIN_LENGTH)
                listStrFinal.add(strTemp);
        }
        return listStrFinal;
    }


    public HashMap<String,List<Properties>> countFrequeny(List<String> listStr,String URL
                                         , String docID){

        for (String word:listStr){
            if (hashWords.containsKey(word)) {
                List<Properties> propertiesList = hashWords.get(word);
                // if exist with this doc id
                if (propertiesList.get(propertiesList.size()-1).docID == docID)
                    propertiesList.get(propertiesList.size()-1).count++ ;
                else{
                    Properties newProperties = new Properties(URL,docID,1);
                    propertiesList.add(newProperties);
                }
            }
            else {
                List<Properties> properties = new ArrayList<>();
                properties.add(new Properties(URL,docID,1));
                hashWords.put(word, properties);
            }
        }
        return hashWords;

    }
    public HashMap<String,List<Properties>> getWords() throws IOException {
        File file = new File("C:\\Users\\MatinB\\Desktop\\University\\Term 7\\Search Engine\\Project Shariat\\textdocs002.txt");
        try {
            FileReader fileReader = new FileReader(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        List<String> stringList = Files.readLines(file, Charsets.UTF_8);
        for (int i = 0 ; i < stringList.size() ; i++){
            // if i == 0
            String docID = stringList.get(i);
            // if i == 1
            i++ ;
            String URL = stringList.get(i);
            // if i ==2
            i++ ;
            String body = stringList.get(i);
            List<String> listWords =  removeBadWords(Splitter.on(" ").trimResults().omitEmptyStrings().splitToList(body));

            countFrequeny(listWords, URL, docID);

            // add to properties
//            Properties properties = new Properties(URL,docID);
        }
        String str = stringList.get(0);


        return hashWords;
    }
}
