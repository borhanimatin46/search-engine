package file_handling;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class SaveLoad {
    File fileWords ;
    Peta_Jensen jensen = new Peta_Jensen();
    HashMap<String,List<Properties>> indexingTable;
    String fileName = "hashList.txt";

    public void getWords(){
        try {
             indexingTable = jensen.getWords();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void saveWords() throws IOException {
        FileOutputStream fileOutputStream = new FileOutputStream(new File(fileName));
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
        objectOutputStream.writeObject(indexingTable);
        fileOutputStream.close();
        objectOutputStream.close();

    }

    public void loadWords() throws IOException {
        FileInputStream fileInputStream = new FileInputStream(new File(fileName));
        ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
        try {
            HashMap<String,List<Properties>> hashMap = (HashMap) objectInputStream.readObject();
            System.out.println();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        System.out.println();

    }
}
