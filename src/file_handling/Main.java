package file_handling;

import java.io.IOException;

public class Main {
    public static void main(String[] args) {
        SaveLoad saveLoad = new SaveLoad();
        saveLoad.getWords();
        // Save
//        try {
//            saveLoad.saveWords();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }

        // Load
        try {
            saveLoad.loadWords();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
